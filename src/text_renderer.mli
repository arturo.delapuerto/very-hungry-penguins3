(** Text renderer module *)

(** display the current game configuration *)
val update_render : unit -> unit

(** ask the user a move on standard input *)
val wait_input : unit -> unit
